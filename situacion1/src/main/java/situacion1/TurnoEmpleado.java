package situacion1;

import java.time.Duration;
import java.time.LocalDateTime;

public class TurnoEmpleado {
    private LocalDateTime fechaHoraIngreso;
    private LocalDateTime fechaHoraEgreso;
    private Empleado empleado;
	public long totalHorasTrabajadas;

    public TurnoEmpleado(LocalDateTime fechaHoraIngreso, LocalDateTime fechaHoraEgreso) {
        this.fechaHoraIngreso = fechaHoraIngreso;
        this.fechaHoraEgreso = fechaHoraEgreso;
        calcularTotalHorasTrabajadas();

    }
    


    public LocalDateTime getFechaHoraIngreso() {
        return fechaHoraIngreso;
    }

    public void setFechaHoraIngreso(LocalDateTime fechaHoraIngreso) {
        this.fechaHoraIngreso = fechaHoraIngreso;
    }

    public LocalDateTime getFechaHoraEgreso() {
        return fechaHoraEgreso;
    }

    public void setFechaHoraEgreso(LocalDateTime fechaHoraEgreso) {
        this.fechaHoraEgreso = fechaHoraEgreso;
    }


    public Empleado getEmpleado() {
        return empleado;
    }


    private void calcularTotalHorasTrabajadas(){   
        totalHorasTrabajadas=totalHorasTrabajadas+Duration.between(fechaHoraIngreso,fechaHoraEgreso).toHours();

     }
    public long getHorasTrabajadas(){
        return totalHorasTrabajadas;
    }
    
    
}
