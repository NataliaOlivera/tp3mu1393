package situacion1;

import java.util.ArrayList;

public class Empleado {
    private String nombre;
    private String apellido;
    private Integer dni;
    private long sueldo;
    private long totalHorasTrabajadas;
    private long costoHoraTrabajo;
    private static ArrayList<TurnoEmpleado> turnosEmpleado;

    public Empleado(String nombre, String apellido, Integer dni, long costoHoraTrabajo) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.dni = dni;
        this.costoHoraTrabajo = costoHoraTrabajo;
        turnosEmpleado = new ArrayList<TurnoEmpleado>();
        calcularSueldo();
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public long getCostoHoraTrabajo() {
        return costoHoraTrabajo;
    }

    public long getSueldo() {
        return sueldo;
    }

    public Integer getDni() {
        return dni;
    }

    public void setDni(Integer dni) {
        this.dni = dni;
    }

    public void incializarArrayListTurnoEmpleado(TurnoEmpleado turnoEmpleado) {
        turnosEmpleado.add(turnoEmpleado);
        calcularSueldo();
        setHorasTrabajadas();
    }

    public  void calcularSueldo() {
        long sueldoParcial=0;
        for(TurnoEmpleado temp: turnosEmpleado){
        sueldoParcial=sueldoParcial+ costoHoraTrabajo*temp.getHorasTrabajadas();
        }
        this.sueldo=sueldoParcial;
    }
    public void setHorasTrabajadas(){
        totalHorasTrabajadas=0;
        for(TurnoEmpleado temp: turnosEmpleado){
            this.totalHorasTrabajadas= totalHorasTrabajadas+ temp.getHorasTrabajadas();
        }
    }

    public long getTotalHorasTrabajadas(){
        return totalHorasTrabajadas;
    }

}
