package situacion1;

import static org.junit.Assert.assertEquals;

import java.time.LocalDateTime;

import org.junit.Test;

public class EmpleadoTest {
    @Test
    public void testTotalHorasTrabajadas(){
        Empleado empleado = new Empleado("Natalia", "Olivera", 2345, 1000);
        TurnoEmpleado turno1= new TurnoEmpleado(LocalDateTime.parse("2020-10-01T06:00:00"),LocalDateTime.parse("2020-10-01T14:00:00"));
        TurnoEmpleado turno2= new TurnoEmpleado(LocalDateTime.parse("2020-10-02T06:00:00"),LocalDateTime.parse("2020-10-02T14:00:00"));
        empleado.incializarArrayListTurnoEmpleado(turno1);
        empleado.incializarArrayListTurnoEmpleado(turno2);
        long actualTotalHoras=empleado.getTotalHorasTrabajadas();
        assertEquals(16, actualTotalHoras);
     }

     @Test

     public void testCalculoDeSueldoDelEmpleado(){
        Empleado empleado = new Empleado("Natalia", "Olivera", 2345, 1000);
        TurnoEmpleado turno1= new TurnoEmpleado(LocalDateTime.parse("2020-10-01T06:00:00"),LocalDateTime.parse("2020-10-01T14:00:00"));
        TurnoEmpleado turno2= new TurnoEmpleado(LocalDateTime.parse("2020-10-02T06:00:00"),LocalDateTime.parse("2020-10-02T14:00:00"));
        empleado.incializarArrayListTurnoEmpleado(turno1);
        empleado.incializarArrayListTurnoEmpleado(turno2);
        long actualSueldo=empleado.getSueldo();
        assertEquals(16000, actualSueldo);
     }
}