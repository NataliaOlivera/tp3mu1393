package situacion2;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import java.time.LocalDateTime;

public class OrdenDeServicioTest {

    @Test
    public void calcularUnaOrdenDeSevicioConVariosProducotos(){
        OrdenDeServicio ordenDeServicio= new OrdenDeServicio(4, 3, "Daniel", LocalDateTime.parse("2020-10-01T08:00:00"),LocalDateTime.parse("2020-10-01T12:00:00"));
        Consumicion consumicion1= new Consumicion("galletas", 30.00, 2);
        Consumicion consumicion2= new Consumicion("jugo", 30.00, 2);
        
        ordenDeServicio.agregarElementos(consumicion1);
        ordenDeServicio.agregarElementos(consumicion2);

        double totalDeLaConsumicion= ordenDeServicio.getTotalO();

        assertEquals(120, totalDeLaConsumicion,0);
    }
}
