package situacion2;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ConsumicionTest {
    @Test
    public void testCalcularTotalDeLaConsumicion(){
        Consumicion consumicion= new Consumicion("cocacola",50.00,2);
        double valorTotalCompra=consumicion.getTotal();
        assertEquals(100, valorTotalCompra,0);

    }
}
