package situacion2;

import java.time.LocalDateTime;
import java.util.ArrayList;

public class OrdenDeServicio {
	private int mesa;
    private int cantidadComensales;
    private String mozo;
    private LocalDateTime fechaHorarioInicio;
    private LocalDateTime fechaHorarioCierre;
    private  ArrayList<Consumicion> consumiciones;
    private Cliente cliente;
    private double total;

    public OrdenDeServicio(int mesa, int cantidadComensales, String mozo, LocalDateTime fechaHorarioInicio,
            LocalDateTime fechaHorarioCierre) {
        this.mesa = mesa;
        this.cantidadComensales = cantidadComensales;
        this.mozo = mozo;
        this.fechaHorarioInicio = fechaHorarioInicio;
        this.fechaHorarioCierre = fechaHorarioCierre;
        consumiciones = new ArrayList<Consumicion>();
        calcularTotalO();
    }

    public int getMesa() {
        return mesa;
    }

    public int getCantidadComensales() {
        return cantidadComensales;
    }

    public String getMozo() {
        return mozo;
    }

    public LocalDateTime getHorarioInicio() {
        return fechaHorarioInicio;
    }

    public LocalDateTime getHorarioCierre() {
        return fechaHorarioCierre;
    }

    public ArrayList<Consumicion> getConsumiciones() {
        return consumiciones;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public void agregarElementos(Consumicion consumicion) {
        consumiciones.add(consumicion);
        calcularTotalO();
    }

    public double calcularTotalO() {
        double totalParcial = 0;
        for (Consumicion con : consumiciones) {
            totalParcial = totalParcial + con.getTotal();
        }
        return this.total = totalParcial;
    }

    public double getTotalO() {
        return total;
    }

    
}
