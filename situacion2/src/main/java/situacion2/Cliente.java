package situacion2;

public class Cliente {
    private  String nombre;
    private  String formaDePago;
    private  OrdenDeServicio ordenDeServicio;

    public Cliente( String nombre,  String formaDePago, OrdenDeServicio ordenDeServicio) {
        this.nombre = nombre;
        this.formaDePago = formaDePago;
        this.ordenDeServicio = ordenDeServicio;
    }

    public String getNombre() {
        return nombre;
    }

    public String getFormaDePago() {
        return formaDePago;
    }

    public void pagoEfectivo(final double total) {
        System.out.println("Pago efectivo igual: " + total);
    }

    // los datos de la tarjeta y del recago, seran ingresados por el mozo
    public void pagoTarjeta(final int datoTarjeta, final int recargo, final double total) {
        System.out.println("Pago tarjeta igual: "+total);
    }

    public OrdenDeServicio getOrdenDeServicio() {
        return ordenDeServicio;
    }

    public void setOrdenDeServicio(OrdenDeServicio ordenDeServicio) {
        this.ordenDeServicio = ordenDeServicio;
    }
}
