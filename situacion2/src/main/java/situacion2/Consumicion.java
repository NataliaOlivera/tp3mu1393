package situacion2;

public class Consumicion {
    private String nombre;
    private double precioUnitario;
    private double cantidad;
    private double total;

    public Consumicion(String nombre, double precioUnitario, double cantidad){
        this.nombre=nombre;
        this.precioUnitario=precioUnitario;
        this.cantidad=cantidad;
        calcularTotal();
    }

    public String getNombre(){
        return nombre;
    }

    public double getPrecioUnitario(){
        return precioUnitario;
    }

    public double getCantidad(){
        return cantidad;
    }

    public double getTotal(){
        return total;
    }

    public void calcularTotal(){
        this.total=cantidad*precioUnitario;
    }
}
